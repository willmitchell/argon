FROM java:8-jdk-alpine
COPY build/libs/argon-0.0.1-SNAPSHOT.jar /app.jar
ENTRYPOINT ["/usr/bin/java"]
CMD ["-jar", "/app.jar"]
EXPOSE 8080