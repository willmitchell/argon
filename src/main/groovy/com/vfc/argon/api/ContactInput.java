package com.vfc.argon.api;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class ContactInput {
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;

    @Pattern(regexp = "\\d{5}(\\-\\d{4})?")
    private String zipcode;

    private String siteId;
    private String address1;
    private String address2;
    private String address3;
    private String city;
    private String state;
    private String email;
    private String phone1;
    private String phone2;
    private String description;
    private String details;
}
