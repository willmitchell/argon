package com.vfc.argon.api;

import lombok.Data;

@Data
public class TransactionResponse {
    String message;
}
