package com.vfc.argon.api;

import lombok.*;

@Data
public class EmployeeResponse {

    private int id;
    private String firstName;
    private String lastName;
    private String username;
    private String brand;
    private String role;
    private String storeId;
}
