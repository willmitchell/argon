package com.vfc.argon.api;

import lombok.Data;

@Data
public class ContactResponse {
    String message;
}
