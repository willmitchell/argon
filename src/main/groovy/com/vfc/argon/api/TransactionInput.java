package com.vfc.argon.api;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

enum TxTypes {TXT_TLOG,TXT_POSLOG}

@Data
public class TransactionInput {
    @NotNull
    private String brandName;
    @NotNull
    private String storeId;
    @NotNull
    private String txId;
    @NotNull
    private String registerId;
    @NotNull
    private TxTypes txType;
    @NotNull
    private String txPayload;
    @NotNull
    private String region;
    @NotNull
    private Date dateTime;
}
