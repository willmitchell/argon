package com.vfc.argon.api;

public interface IContactService {
    ContactResponse contact(ContactInput input);
}
