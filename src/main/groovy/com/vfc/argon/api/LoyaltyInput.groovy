package com.vfc.argon.api

class LoyaltyInput {
    String email
    int storeId
    Date date
    String txId
    String brand
    String region
}
