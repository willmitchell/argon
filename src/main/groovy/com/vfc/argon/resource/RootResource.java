package com.vfc.argon.resource;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class RootResource {

    @GetMapping
    ResponseEntity<String> rootGet() {
        return ResponseEntity.ok("Status: UP");
    }

}
