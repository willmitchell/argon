package com.vfc.argon.resource;

import com.vfc.argon.api.EmployeeResponseSet;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/employee")
@Data
public class EmployeeResource {

    public static final String JSON = "application/json";

    @GetMapping(produces = JSON)
    ResponseEntity<EmployeeResponseSet>
    get() {
        return ResponseEntity.ok(new EmployeeResponseSet());
    }

}
