package com.vfc.argon.resource;

import com.vfc.argon.api.EmployeeResponseSet;
import com.vfc.argon.api.LoyaltyInput;
import com.vfc.argon.api.LoyaltyResponse;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/loyalty")
@Data
public class LoyaltyResource {

    public static final String JSON = "application/json";

    @PostMapping(produces = JSON, consumes = JSON)
    ResponseEntity<LoyaltyResponse>
    lookup(LoyaltyInput input) {
        return ResponseEntity.ok(new LoyaltyResponse());
    }

}
