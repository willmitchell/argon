package com.vfc.argon.resource;

import com.vfc.argon.api.ContactInput;
import com.vfc.argon.api.ContactResponse;
import com.vfc.argon.api.IContactService;
import lombok.Data;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1/contact")
@Data
public class ContactResource {

    @Autowired
    IContactService contactService;

    @PostMapping
    ContactResponse contact(@RequestBody @Valid ContactInput input) {
        return contactService.contact(input);
    }

}
