package com.vfc.argon.resource;

import com.vfc.argon.api.*;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1/transaction")
@Data
public class TransactionResource {

    public static final String JSON = "application/json";

    @PostMapping(consumes = {JSON},produces = JSON)
    TransactionResponse create(@RequestBody @Valid TransactionInput input) {
        return new TransactionResponse();
    }

}
