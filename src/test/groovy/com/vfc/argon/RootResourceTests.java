package com.vfc.argon;

import com.vfc.argon.resource.RootResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = RootResource.class, secure = false)
@WebAppConfiguration
public class RootResourceTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void testContact0() throws Exception {
        mockMvc.perform(get("/v1/")).andExpect(status().isOk());
    }
}