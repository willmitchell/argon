package com.vfc.argon;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vfc.argon.api.ContactInput;
import com.vfc.argon.resource.ContactResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ContactResource.class, secure = false)
@WebAppConfiguration
public class EmployeeResourceTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void testGetEmployeeSet() throws Exception {

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/v1/employee")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk());
    }
}