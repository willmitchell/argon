package com.vfc.argon;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vfc.argon.api.ContactInput;
import com.vfc.argon.resource.ContactResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ContactResource.class, secure = false)
@WebAppConfiguration
public class ContactResourceTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void testContact0() throws Exception {
        ContactInput ci = new ContactInput();
        ci.setFirstName("joe");
        ci.setLastName("smith");
        ci.setZipcode("24540");

        ObjectMapper objectMapper = new ObjectMapper();
        String s = objectMapper.writer().writeValueAsString(ci);
        System.out.println("s = " + s);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/v1/contact")
                .accept(MediaType.APPLICATION_JSON).content(s)
                .contentType(MediaType.APPLICATION_JSON);



        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk());

//                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
//                .andExpect(jsonPath("$", hasSize(2)))
//                .andExpect(jsonPath("$[0].id", is(1)))
//                .andExpect(jsonPath("$[0].description", is("Lorem ipsum")))
//                .andExpect(jsonPath("$[0].title", is("Foo")))
//                .andExpect(jsonPath("$[1].id", is(2)))
//                .andExpect(jsonPath("$[1].description", is("Lorem ipsum")))
//                .andExpect(jsonPath("$[1].title", is("Bar")));

//        verify(todoServiceMock, times(1)).findAll();
//        verifyNoMoreInteractions(todoServiceMock);
    }
}