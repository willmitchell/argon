# Project Argon: VF API for Point of Sale

- Spring boot
- Swagger (via Spring Fox)
- Spring MVC
- Testable
- Not necessarily monolithic
- Docker
- Gradle

# Usage

./gradlew bootRun # runs the app
./gradlew bootRepackage # builds fat jar
./bin/build-docker.sh # builds Docker image

