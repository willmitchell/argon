#!/bin/bash -xe

echo generating fat jar
./gradlew bootRepackage

echo building docker image
docker build -t argon .
