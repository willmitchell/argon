#!/bin/bash -xe

echo getting ECR docker credentials
eval $(aws ecr get-login --no-include-email --region us-east-1)

echo applying repo-specific tag to local build of argon
docker tag argon:latest 331834852303.dkr.ecr.us-east-1.amazonaws.com/argon:latest

echo now push that tag and binaries to the remote
docker push 331834852303.dkr.ecr.us-east-1.amazonaws.com/argon:latest
