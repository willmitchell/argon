#!/bin/bash -xe

aws iam --region us-east-1 create-role --role-name ecsExecutionRole --assume-role-policy-document file://./config/execution-assume-role.json